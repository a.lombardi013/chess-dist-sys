# chess-dist-sys

web application chess game

What you need to get things up and running:
1.  Clone Branch from GitLab (this project uses Sprint Boot)
2.  Install [Node.js](https://nodejs.org/en/download/)  and [NPM](https://www.npmjs.com/get-npm)
3.  Install [Angular CLI](https://angular.io/guide/quickstart)  
4.  You will also need an [Java IDE](https://www.jetbrains.com/idea/) for the backend and a [Code Editor](https://code.visualstudio.com/) for the frontend
5.  This project uses Java 8, make sure you have this version installed

How to build the project:
1.  Once you have the project cloned open it via your prefered Java IDE
2.  Make sure your Maven dependencies are up to date
3.  Run the project with com.chess.controller.MoveController as the main class
3.  Open the chess-client folder in your prefered Code Editor
4.  Open your CMD/Terminal in the chess-client directory and run 
>  npm install
5.  Once the node packages are all up to date run this also in the CMD/Terminal
>  ng serve --open
